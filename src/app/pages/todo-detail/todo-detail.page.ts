import { Component, OnDestroy, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data/data.service';
import { Todo } from 'src/app/shared/todo';

@Component({
  selector: 'app-todo-detail',
  templateUrl: './todo-detail.page.html',
  styleUrls: ['./todo-detail.page.scss'],
})
export class TodoDetailPage implements OnInit ,OnDestroy {
  todo : Todo;
  constructor(
    private dataService: DataService
  ) { }

  ngOnInit() {
    this.todo=this.dataService.getParams().todo;
  }
  ngOnDestroy(){
    this.dataService.setParams({});
  }

}
