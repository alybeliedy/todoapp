import { Component, OnInit } from '@angular/core';
import { AlertController, NavController } from '@ionic/angular';
import { DataService } from 'src/app/services/data/data.service';
import { Todo } from 'src/app/shared/todo';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
 todos:Todo[];
 loading :boolean=true;
  constructor(
    private navCtr: NavController,
    private alertCtrl:AlertController,
    private dataService:DataService
  ) { }

  ngOnInit() {
    this.getData();
  }

  createTodo()
    {
       this.navCtr.navigateForward('/todo-config');
    }

    showDetails(todo:Todo)
    {
      this.dataService.setParams({
        todo
      });
      this.navCtr.navigateForward('/todo-detail');
    }

    getData()
    {
      setTimeout(() => {
        this.todos=this.dataService.getData();  
         this.loading=false;
      }, 3000);
    }

   async delete(index:number)
    {
       let alert= await this.alertCtrl.create({
          header:'Confirm Deleting',
          message:'are you sure ?',
          mode:'ios',
          buttons:[
            {
             text:'No',
             role:'cancel' 
            },
            {
              text:'Yes',
              handler:()=>{
                console.log('deleting todo');
                this.todos.splice(index,1); 
                
              }
            }

          ]
       }); 
       await alert.present();
      }

      edit(todo:Todo){
         this.dataService.setParams({
           todo
         });
         this.navCtr.navigateForward('/todo-config');
      }

      refreshPage(ev)
      {
        setTimeout(() => {
          this.todos=this.dataService.getData();  
          ev.target.complete();
        }, 3000);
      }
      loadData(ev)
      {
        setTimeout(() => {
          this.todos=this.todos.concat(this.dataService.getData());
          ev.target.complete();
        }, 3000);
      }
}
