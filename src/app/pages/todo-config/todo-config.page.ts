import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { DataService } from 'src/app/services/data/data.service';
import { Todo } from 'src/app/shared/todo';

@Component({
  selector: 'app-todo-config',
  templateUrl: './todo-config.page.html',
  styleUrls: ['./todo-config.page.scss'],
})
export class TodoConfigPage implements OnInit {
  form:FormGroup;
  todo :Todo;
  constructor(
    private fb:FormBuilder,
    private navCtrl:NavController,
    private dataService:DataService
  ) { 
    this.createTodo();
  }

  ngOnInit() {
    this.todo=this.dataService.getParams().todo;
    this.patchForm();
  }
   //this method check if there todo create before
 patchForm(){
   if(this.todo){
       this.form.patchValue({
         title:this.todo.title,
         desc:this.todo.desc
       })
   }
 }

  createTodo(){
    this.form=this.fb.group({
      title:['',Validators.required],
      desc:['',Validators.required]
    })
  }

  saveTodo(){
    console.log(this.form.value);
    let form=this.form.value;
    if(this.todo){
      this.todo.title=form.title;
      this.todo.desc=form.desc;
      this.todo.date=new Date();
    }
    this.navCtrl.pop();
    
  }

}
